<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TrailerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Trailer;

/**
* @Route("/trailer", name="trailer")
*/
class TrailerController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function index(TrailerRepository $repo)
    {

        $trailer = $repo->findAll();
        $json = $this->serializer->serialize($trailer, 'json');
        return new JsonResponse($json, JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Route(methods="POST")
     */
    public function addTrailer(Request $request, ObjectManager $manager)
    {
        $trailer = new Trailer();
        $form = $this->createForm(TrailerType::class, $trailer);

        $form->submit(json_decode($request->getContent(), true));

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $manager->persist($trailer);
            $manager->flush();
            return new JsonResponse($this->serializer->serialize($trailer, 'json'), JsonResponse::HTTP_CREATED, [], true);
        }
        return $this->json($form->getErrors(true), JsonResponse::HTTP_BAD_REQUEST);
    }
    /**
    * @Route("/{trailer}", methods="GET")
    */
    public function findByOne(Trailer $trailer)
    {
        return new JsonResponse($this->serializer->serialize($trailer, 'json'), JsonResponse::HTPP_OK, [], true);
    }

    /**
     * @Route("/{trailer}", methods="DELETE")
     */
    public function delete(Trailer $trailer, ObjectManager $manager)
    {
        $manager->remove($trailer);
        $manager->flush();
        return $this->json("", JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{trailer}", methods="PATCH")
     */
    public function update(Trailer $trailer, Request $request, ObjectManager $manager)
    {
        $form = $this->createForm(TrailerType::class, $trailer);
        $form->submit(json_decode($request->getContent(), true), false);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $manager->flush();
            return new JsonResponse($this->serializer->serialize($trailer, 'json'), JsonResponse::HTTP_OK, [], true);
        }
        return $this->json($form->getErrors(true), JsonResponse::HTTP_BAD_REQUEST);
    }
}
