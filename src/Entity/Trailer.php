<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrailerRepository")
 */
class Trailer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $serieName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $serieType;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbSeason;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbEpisode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerieName(): ?string
    {
        return $this->serieName;
    }

    public function setSerieName(string $serieName): self
    {
        $this->serieName = $serieName;

        return $this;
    }

    public function getSerieType(): ?string
    {
        return $this->serieType;
    }

    public function setSerieType(string $serieType): self
    {
        $this->serieType = $serieType;

        return $this;
    }

    public function getNbSeason(): ?int
    {
        return $this->nbSeason;
    }

    public function setNbSeason(int $nbSeason): self
    {
        $this->nbSeason = $nbSeason;

        return $this;
    }

    public function getNbEpisode(): ?int
    {
        return $this->nbEpisode;
    }

    public function setNbEpisode(int $nbEpisode): self
    {
        $this->nbEpisode = $nbEpisode;

        return $this;
    }
}
