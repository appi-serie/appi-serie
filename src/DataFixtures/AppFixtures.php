<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Trailer;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($x = 1; $x <= 7; $x++) {

        $trailer = new Trailer();
        $trailer->setSerieName('Arrow'.$x);
        $trailer->setSerieType('Action');
        $trailer->setNbSeason($x);
        $trailer->setNbEpisode($x*10);
     
        $manager->persist($trailer);


        $manager->flush();

        }
    }
}
