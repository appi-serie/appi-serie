# Appi Serie

# Blog Simplon

## Présentation du projet

Le but de ce projet est de créer une API pour un blog de critique de séries, notre API propose de visionner les "trailers" des séries.

## Languages utilisées

* **HTML 5**
* **PHP 7**
* **MySQL**

## Interet

1. utilisation et apprentissage de **SYMFONY 4**, **DOCTRINE**, **PHP 7** et utilisation des API.

## Exemple de code utilisé

![20% center](./doc/controller.png)

## Diagramme Use Case

![20% center](./doc/uml.png)

## Difficultés

Le temps pour réalisé l'exercice était trés court.

## Axes d'amélioration

## Ce que j'ai aimé faire

## Pour lancer mon projet

1. Cloner le projet
2. Installer les dépendances avec composer install
3. Créer un fichier .env.local avec dedans DATABASE_URL=mysql://user:password@127.0.0.1:3306/db_name en remplaçant user, password et db_name par vos informations de connexion à mariadb
4. Faire un bin/console doctrine:migrations:migrate pour mettre la bdd dans le bon état (si jamais ça marche pas, tenter de faire un bin/console doctrine:schema:drop --force avant de refaire le migrate pour remettre à zéro la bdd)

## Copyright

©2018-2019 Lirisata & Benid All right reserved.
